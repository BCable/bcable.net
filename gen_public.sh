#!/bin/bash

mkdir -p public/template

find . \
	-maxdepth 1 \
	! -name pages \
	! -name public \
	! -name template \
	! -name \*.info \
	! -name \*.R \
	! -name \*.sh \
	! -name LICENSE \
	! -name README.md \
	! -name Makefile \
	! -name .\* -or -name .gitlab-ci.yml | \
	xargs -I{} cp -a {} public

find template -name \*.css | xargs -I{} cp -a {} public/template
