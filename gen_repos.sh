#!/bin/bash

repofile="repos.html"

cat template/header-*.html > "$repofile"

title="Repositories"
sed -r 's@^(<title>Brad Cable)(</title>)$@\1 - '"${title}"'\2@g' -i "$repofile"

cat repos.info | while read repo; do
	if [[ "${repo:0:1}" = "#" ]]; then
		continue

	elif [[ "${repo:${#repo}-3}" = ",,," ]]; then
		if [[ "${repo}" = ",,,,," ]]; then
			echo -e "</table>\n" >> "$repofile"
			continue
		fi

		if [[ "${repo:0:2}" != ",," ]]; then
			header="${repo:0:${#repo}-3}"
		else
			echo -e "<table class=\"repo\" cellpadding=\"2\">" >> "$repofile"
			header="${repo:2:${#repo}-5}"
		fi

		echo -e "<tr><th colspan=\"3\">${header}</th></tr>\n" >> "$repofile"

	else
		name="$(echo "$repo" | cut -d ',' -f1)"
		lang="$(echo "$repo" | cut -d ',' -f2)"
		version="$(echo "$repo" | cut -d ',' -f3)"
		functional="$(echo "$repo" | cut -d ',' -f4)"
		demo="$(echo "$repo" | cut -d ',' -f5)"

		if [[ "$functional" == "1" ]]; then
			echo -e "<tr class=\"maintained\">" >> "$repofile"
		elif [[ "$functional" == "0" ]]; then
			echo -e "<tr class=\"functional\">" >> "$repofile"
		else
			echo -e "<tr class=\"deprecated\">" >> "$repofile"
		fi
		echo -e "<td class=\"namelink\">" >> "$repofile"
		echo -e "<a href=\"https://bcable.net/x/${name}\">" >> "$repofile"
		echo -e "${name}</a>" >> "$repofile"
		if [[ ! -z "$demo" ]]; then
			echo -e "&nbsp;(<a href=\"${demo}\">demo</a>)" >> "$repofile"
		fi
		echo -e "</td>" >> "$repofile"
		echo -e "<td class=\"version\">${version}</td>" >> "$repofile"
		echo -e "<td class=\"language\">(${lang})</td>" >> "$repofile"
		echo -e "</tr>\n" >> "$repofile"
	fi
done

echo -e "</table>" >> "$repofile"
cat template/footer.html >> "$repofile"
