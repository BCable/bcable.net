#!/bin/bash

DUMP_CSV=0
if [[ "$1" = "--dump-csv" ]]; then
	DUMP_CSV=1
	shift
fi

function gitlog_csv(){
	cd "$1"

	curdir="$(pwd)"
	projname="$(basename "$curdir")"

	git log | grep -E "^[^ ]" | awk '
		/^commit/{
			merge = "";
			author = "";
			email = "";
			merge = "";
			commit = $2;
			getline;
		}
		/^Author/{
			author = gensub("^Author: ([^<]+) .*$", "\\1", "g");
			email = gensub("^Author: [^<]+ <([^>]+)>.*$", "\\1", "g");
			getline;
		}
		/^Merge/ {
			merge = gensub("^Merge: (.+)$", "\\1", "g");
		}
		/^Date/{
			date = gensub("^Date:[ \t]+(.*)$", "\\1", "g");
			print "\"" date "\",\"" author "\",\"" email "\",\"" \
				commit "\",\"" merge "\"";
		}' | sed -r "s/^/\"${projname}\",/g"

	cd - &> /dev/null
}

function csv_out(){
	echo "Repository,Date,Author,Email,Commit,Merge"
	while [[ ! -z "$1" ]]; do
		gitlog_csv "$1" 2> /dev/null
		shift
	done
}

if [[ "$DUMP_CSV" = "1" ]]; then
	csv_out $@
	exit 0
fi

csv_out $@ | R -q -f gitlog.R

ls -1 gitlog-*.png | while read png; do
	mogrify -flip "$png"
done

mkdir -p images/gitlog
mv gitlog-20*.png images/gitlog
