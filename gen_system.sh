#!/bin/bash

ls -1 system | grep -v system-versions | while read line; do
	cat template/header-00.html > "$line"
	echo '<link rel="stylesheet" href="template/system.css" />' >> "$line"
	cat template/header-01.html >> "$line"
	bash alter_title.sh "My Awesome Secure and Portable System (2009)" "$line"

	cat "system/$line" >> "$line"
	cat system/system-versions.html >> "$line"
	cat template/footer.html >> "$line"
done
