all: favicon.ico gen_analysis gen_gitlog gen_pages gen_repos gen_system gen_techart

clean:
	rm -f *.html *.js *.pdf
	rm -f favicon.ico
	rm -f Rplots.pdf
	rm -f MathJax.js
	rm -rf data docs images/analysis images/gitlog public

bcable.net:
	bash gen_public.sh

favicon.ico:
	convert -resize 100x100 images/golden-apple-uni-small.png favicon.ico

gen_analysis:
	bash gen_analysis.sh

gen_gitlog:
	bash gen_gitlog.sh ../*

gen_pages:
	bash gen_pages.sh

gen_repos:
	bash gen_repos.sh

gen_system:
	bash gen_system.sh

gen_techart:
	bash gen_techart.sh
