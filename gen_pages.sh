#!/bin/bash

ls -1 pages | while read line; do
	cat template/header-*.html > "$line"

	title="$(echo "$line" | \
		sed "s/index.html/Main/g" | \
		sed "s/analysis.html/Analysis/g"
	)"
	bash alter_title.sh "$title" "$line"

	cat "pages/$line" >> "$line"
	cat template/footer.html >> "$line"

	echo "$title: done"
done
