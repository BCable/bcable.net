#!/bin/bash

# old MathJax
# https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML
# new MathJax
MATHJAX_PATH="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js"

WEB_DIR="$(pwd)"
GIT_DIR="$(pwd)/.."
tempdir="$(mktemp -d /tmp/gen_analysis.XXXXXXXXXX)"
cd "$tempdir"

# grab gh-pages branches needed
git clone --branch gh-pages "$GIT_DIR/gimp"
git clone --branch gh-pages "$GIT_DIR/Rproj"
git clone --branch gh-pages "$GIT_DIR/pygaR"

# grab MathJax
wget -O $WEB_DIR/MathJax.js "$MATHJAX_PATH"

# function for generating a single page
function gen(){
	outfile="$WEB_DIR/$1"
	shift
	dir="$1"
	shift

	if [[ ! -z "$1" ]]; then
		file="$1"
	else
		file="index"
	fi
	shift

	# build file
	cat "${WEB_DIR}/template/header-"* > "$outfile.html"
	grep -vE "^(<\!DOCTYPE|<html>|<head>|<meta|<title>|</head|<body>)" \
		"$dir/$file.html" >> "$outfile.html"
	cat "${WEB_DIR}/template/footer.html" >> "$outfile.html"

	# grab title
	# rmarkdown v2
	title="$(cat "$outfile.html" | awk '
		BEGIN{ doprint=0; };
		//{
			if(doprint == 1){
				if(index($0, "</h1>") != 0){
					doprint=0;
					print gensub(/^(.*)<\/h1>.*$/, "\\1", "g", $0);
				} else {
					print $0;
				};
			};
		};
		/<h1 class="title/{
			print $0;
			doprint=1;
		};' | tr '\n' ' ' | sed -r -e "s/[ ]+$//g" -e "s@^<[^>]+>@@g"
	)"
	if [[ -z "$title" ]]; then
		# rmarkdown v1
		title="$(grep -m 1 "<h1>" "$outfile.html" | sed -r "s@<[^>]+>@@g")"
	fi

	# clean up file
	bash "${WEB_DIR}/alter_title.sh" "$title" "$outfile.html"
	sed -r 's@https://[^"]+MathJax.js[^"]*@MathJax.js@g' -i "$outfile.html"
	awk '
		BEGIN { instyle=0; }
		/^<style/{ instyle=1; }
		//{ if(instyle == 0){ print $0; }}
		/^<\/style>/{ instyle=0; }
	' "$outfile.html" > "$outfile.html.awkout"
	mv "$outfile.html.awkout" "$outfile.html"

	echo "$title: done"
}

# create new analysis page files
gen analysis-pygar_form pygaR/pygar_form
gen analysis-pygar_master pygaR/pygar_master
gen analysis-pygar_search pygaR/pygar_search
gen analysis-booktree Rproj/booktree
gen analysis-central_limit Rproj/central_limit
gen analysis-delegates Rproj/delegates
gen analysis-devops Rproj/devops
gen analysis-dicejobs Rproj/dicejobs
gen analysis-github_R Rproj/github_R
gen analysis-historical_reps Rproj/historical_reps
gen analysis-humanact_ml Rproj/humanact_ml
gen analysis-mandelbrot Rproj/mandelbrot
gen analysis-npd_dailylog Rproj/npd/dailylog
gen analysis-ozone Rproj/ozone
gen analysis-qi_scores Rproj/qi_scores
gen analysis-storm_data Rproj/storm_data
gen analysis-httpd_world Rproj/httpd/world
gen analysis-httpd_world_go-2019 Rproj/httpd/world_go-2019
gen analysis-httpd_world_go-2020 Rproj/httpd/world_go-2020
gen analysis-syslog_iptables_world Rproj/syslog/iptables_world
gen analysis-syslog_iptables_world-2019 Rproj/syslog/iptables_world-2019
gen analysis-syslog_iptables_world_go-2019 Rproj/syslog/iptables_world_go-2019
gen analysis-syslog_iptables_world_go-2020 Rproj/syslog/iptables_world_go-2020
gen analysis-syslog_iptables_world_go-2023 Rproj/syslog/iptables_world_go-2023
gen analysis-Rsgf_moves_anim Rproj/sgf/moves_anim
gen analysis-Rsgf_period_cards Rproj/sgf/period_cards
gen analysis-Rsgf_player_card Rproj/sgf/player_card
gen analysis-syslog_iran_2020 Rproj/syslog/iran_2020
gen analysis-gimppy_simple_objects_grow gimp/simple_objects
gen analysis-ok_email-state_map Rproj/ok_email/state_map
gen analysis-epikfail-contacts_map Rproj/epikfail/contacts_map
gen analysis-httpd-log4j_obfuscate Rproj/httpd/log4j_obfuscate
gen analysis-httpd-log4j_rawlogs Rproj/httpd/log4j_rawlogs
gen analysis-httpd-log4j_ircc2c Rproj/httpd/log4j_ircc2c
gen analysis-ukr-prelim Rproj/ukr/prelim
gen analysis-ukr-graphs Rproj/ukr/graphs
gen analysis-ukr-indicators Rproj/ukr/indicators
gen analysis-ukr-ru_map_sessions Rproj/ukr/ru_map_sessions
gen analysis-ukr-cn_map_sessions Rproj/ukr/cn_map_sessions
gen analysis-ukr-miori_fail Rproj/ukr/miori_fail
gen analysis-ukr-botnet_perl Rproj/ukr/botnet_perl
gen analysis-ukr-ddos_gh0st Rproj/ukr/ddos_gh0st
gen analysis-ukr-indicators_2023 Rproj/ukr/indicators_2023
gen analysis-ukr-crew_001 Rproj/ukr/crew_001
gen analysis-ukr-inventory_attack Rproj/ukr/inventory_attack
gen analysis-ukr-crew_002 Rproj/ukr/crew_002
gen analysis-httpd_world_go-2023 Rproj/httpd/world_go-2023
gen analysis-ukr-graphs_go-2022 Rproj/ukr/graphs_go-2022
gen analysis-ukr-graphs_go-2023 Rproj/ukr/graphs_go-2023
gen analysis-ukr-crew_003 Rproj/ukr/crew_003

# handle GIFs (syslog_iptables_world-2019)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world-2019"
cp \
	Rproj/syslog/iptables_world-2019/anim_common_ports_render.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world-2019"
sed \
	-r "s@(anim_common_ports_render\.gif)@images/analysis/syslog_iptables_world-2019/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world-2019.html"

# handle GIFs (syslog_iptables_world_go-2019)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world_go-2019"
cp \
	Rproj/syslog/iptables_world_go-2019/anim_common_ports_render.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world_go-2019"
sed \
	-r "s@(anim_common_ports_render\.gif)@images/analysis/syslog_iptables_world_go-2019/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world_go-2019.html"

# handle GIFs (syslog_iptables_world_go-2020)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world_go-2020"
cp \
	Rproj/syslog/iptables_world_go-2020/anim_ports_org.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world_go-2020"
sed \
	-r "s@(anim_ports_org\.gif)@images/analysis/syslog_iptables_world_go-2020/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world_go-2020.html"

# handle GIFs (syslog_iptables_world_go-2020)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world_go-2020"
cp \
	Rproj/syslog/iptables_world_go-2020/anim_common_ports_render.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world_go-2020"
sed \
	-r "s@(anim_common_ports_render\.gif)@images/analysis/syslog_iptables_world_go-2020/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world_go-2020.html"

# handle GIFs (syslog_iptables_world_go-2023)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world_go-2023"
cp \
	Rproj/syslog/iptables_world_go-2023/anim_ports_org.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world_go-2023"
sed \
	-r "s@(anim_ports_org\.gif)@images/analysis/syslog_iptables_world_go-2023/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world_go-2023.html"

# handle GIFs (syslog_iptables_world_go-2023)
mkdir -p "${WEB_DIR}/images/analysis/syslog_iptables_world_go-2023"
cp \
	Rproj/syslog/iptables_world_go-2023/anim_common_ports_render.gif \
	"${WEB_DIR}/images/analysis/syslog_iptables_world_go-2023"
sed \
	-r "s@(anim_common_ports_render\.gif)@images/analysis/syslog_iptables_world_go-2023/\1@g" \
	-i "${WEB_DIR}/analysis-syslog_iptables_world_go-2023.html"

# handle GIFs and MP4s (Rsgf_moves_anim)
mkdir -p "${WEB_DIR}/images/analysis/Rsgf_moves_anim"
cp \
	Rproj/sgf/moves_anim/out/* \
	"${WEB_DIR}/images/analysis/Rsgf_moves_anim"
sed \
	"s@\"out/anim_@\"images/analysis/Rsgf_moves_anim/anim_@g" \
	-i "${WEB_DIR}/analysis-Rsgf_moves_anim.html"

# handle PNGs (Rsgf_period_cards)
mkdir -p "${WEB_DIR}/images/analysis/Rsgf_period_cards"
cp \
	Rproj/sgf/period_cards/*.png \
	"${WEB_DIR}/images/analysis/Rsgf_period_cards"
sed \
	-r "s@((final|plot)_plot3d_[a-z90-9_]*\.png)@images/analysis/Rsgf_period_cards/\1@g" \
	-i "${WEB_DIR}/analysis-Rsgf_period_cards.html"

# handle PDFs (Rsgf_player_card)
mkdir -p "${WEB_DIR}/docs/Rsgf"
cp Rproj/sgf/player_card/Rsgf-player_card.pdf "$WEB_DIR/docs/Rsgf"
sed -r "s@(Rsgf-player_card\.pdf)@docs/Rsgf/\1@g" -i "$WEB_DIR/analysis-Rsgf_player_card.html"

# handle GIFs and MP4s (GIMP-Python Simple Objects)
mkdir -p "${WEB_DIR}/images/analysis/gimp_simple_objects"
cp \
	gimp/simple_objects/anim/* \
	"$WEB_DIR/images/analysis/gimp_simple_objects"
sed \
	-r "s@anim/@images/analysis/gimp_simple_objects/@g" \
	-i "${WEB_DIR}/analysis-gimppy_simple_objects_grow.html"

# handle log4j files
mkdir -p "${WEB_DIR}/data/analysis/log4j"
cp Rproj/httpd/log4j_rawlogs/attack_ip.csv "${WEB_DIR}/data/analysis/log4j"
cp Rproj/httpd/log4j_rawlogs/beacon_servers.csv "${WEB_DIR}/data/analysis/log4j"
cp Rproj/httpd/log4j_ircc2c/pas* "${WEB_DIR}/data/analysis/log4j"
cp "${GIT_DIR}/Rproj/httpd/log4j_rawlogs/log4j_httpd.txt" \
	"${WEB_DIR}/data/analysis/log4j"
sed \
	-r 's@href="(pas[3]?)"@href="data/analysis/log4j/\1"@g' \
	-i "${WEB_DIR}/analysis-httpd-log4j_ircc2c.html"

# handle ukr files
mkdir -p "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/prelim/*.png "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/graphs/*.csv "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/indicators/*.csv "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/ru_map_sessions/*.csv "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/cn_map_sessions/*.csv "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/miori_fail/*.csv.gz "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/indicators_2023/*.csv "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/crew_001/*.{png,gif,tar.xz} "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/inventory_attack/*.png "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/crew_002/*.png "${WEB_DIR}/data/analysis/ukr"
cp Rproj/ukr/crew_003/*.{png,jpg} "${WEB_DIR}/data/analysis/ukr"
cp -a Rproj/ukr/crew_003/paradise "${WEB_DIR}/data/analysis/ukr"
sed \
	-r 's@href="([^\"]*\.png)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-prelim.html"
sed \
	-r 's@href="([^\"]*\.csv)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-"*.html
sed \
	-r 's@href="([^\"]*\.csv)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-indicators_2023.html"
sed \
	-r 's@href="([^\"]*\.(png|gif))"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-crew_001.html"
sed \
	-r 's@href="([^\"]*\.tar\.xz)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-crew_001.html"
sed \
	-r 's@href="([^\"]*\.png)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-inventory_attack.html"
sed \
	-r 's@href="([^\"]*\.png)"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-crew_002.html"
sed \
	-r 's@href="([^\"]*\.(png|jpg))"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-crew_003.html"
sed \
	-r 's@href="(paradise/[^\"]*\.(png|mp4))"@href="data/analysis/ukr/\1"@g' \
	-i "${WEB_DIR}/analysis-ukr-crew_003.html"

# clean up
rm -rf "$tempdir"
