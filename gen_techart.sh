#!/bin/bash

cat template/header-*.html > techart.html
bash alter_title.sh "Tech Art" techart.html

evenodd=0
cat techart.info | while read artproj; do
	projname="$(echo "$artproj" | cut -d ',' -f1)"
	projtitle="$(echo "$artproj" | cut -d ',' -f2)"
	projcolsize="$(echo "$artproj" | cut -d ',' -f3)"
	projlink="$(echo "$artproj" | cut -d ',' -f4)"

	if [[ "$evenodd" = 0 ]]; then
		evenodd=1
		echo '<div class="taproj clrb">' >> techart.html
	else
		evenodd=0
		echo '<div class="taproj">' >> techart.html
	fi

	# individual page
	cat template/header-*.html > "techart-${projname}.html"
	echo '<div class="taproj">' >> "techart-${projname}.html"
	bash alter_title.sh "$projtitle" "techart-${projname}.html"

	# main page
	echo "<h1><a href=\"techart-${projname}.html\">${projtitle}</a></h1>" >> techart.html
	echo "<div class=\"taprevcol\">" >> techart.html
	echo "<img class=\"tamainthumb\" src=\"techart/${projname}/${projname}-00.jpg\" />" >> techart.html

	# individual page
	echo "<h1>${projtitle}</h1>" >> "techart-${projname}.html"
	echo "<div class=\"taprevcol\">" >> "techart-${projname}.html"
	echo "<img class=\"tamainthumb\" src=\"techart/${projname}/${projname}-00.jpg\" />" >> "techart-${projname}.html"

	if [[ ! -z "$projlink" ]]; then
		echo "<center>(<a href=\"${projlink}\">source</a>)</center>" >> techart.html
		echo "<center>(<a href=\"${projlink}\">source</a>)</center>" >> "techart-${projname}.html"
	fi

	echo "</div>" >> techart.html
	echo "</div>" >> "techart-${projname}.html"

	if [[ "$projcolsize" = "3" ]]; then
		echo '<div class="tathumbcol300">' >> techart.html
		echo '<div class="tathumbcol300">' >> "techart-${projname}.html"
	elif [[ "$projcolsize" = "2" ]]; then
		echo '<div class="tathumbcol200">' >> techart.html
		echo '<div class="tathumbcol200">' >> "techart-${projname}.html"
	else
		echo '<div class="tathumbcol">' >> techart.html
		echo '<div class="tathumbcol">' >> "techart-${projname}.html"
	fi

	# images
	ls -1 "techart/${projname}/${projname}-thumb-"* | while read thumb; do
		fullimg="$(echo "$thumb" | sed "s/-thumb-/-/")"
		if [[ ! -e "$fullimg" ]]; then
			fullimg="$(echo "$fullimg" | sed "s/jpg/png/")"
		fi

		# main page
		echo -n "<a href=\"${fullimg}\">" >> techart.html
		echo -n "<img src=\"${thumb}\" />" >> techart.html
		echo -n "</a>" >> techart.html

		# individual page
		echo -n "<a href=\"${fullimg}\">" >> "techart-${projname}.html"
		echo -n "<img src=\"${thumb}\" />" >> "techart-${projname}.html"
		echo -n "</a>" >> "techart-${projname}.html"
	done

	# video
	ls -1 "techart/${projname}/${projname}-vthumb-"* | while read thumb; do
		fullvid="$(echo "$thumb" | sed "s/-vthumb-/-/" | sed "s/jpg/mp4/")"

		# main page
		echo -n "<a href=\"${fullvid}\">" >> techart.html
		echo -n "<img src=\"${thumb}\" />" >> techart.html
		echo -n "</a>" >> techart.html

		# individual page
		echo -n "<a href=\"${fullvid}\">" >> "techart-${projname}.html"
		echo -n "<img src=\"${thumb}\" />" >> "techart-${projname}.html"
		echo -n "</a>" >> "techart-${projname}.html"
	done

	echo '</div></div>' >> techart.html
	echo '</div></div>' >> "techart-${projname}.html"

	# individual footer
	cat template/footer-techart.html >> "techart-${projname}.html"
	cat template/footer.html >> "techart-${projname}.html"
done

cat template/footer-techart.html >> techart.html
cat template/footer.html >> techart.html
