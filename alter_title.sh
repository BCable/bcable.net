#!/bin/bash

title="$1"
file="$2"

sed -r 's@^(<title>Brad Cable)(</title>)$@\1 - '"${title}"'\2@g' -i "$file"
